#!/bin/sh

tpl="newsletter.html.tpl"
newsletter="newsletter.html"

cp "$tpl $newsletter"

ascfile="tmp.asc"
for i in {1..9} a b c
#for i in 8
do
    pngfile="${i}.png"
    if [ ! -f "$pngfile" ]; then
        echo "no $pngfile found, searching ${i}.jpg"
        pngfile="${i}.jpg"
        if [ ! -f "$pngfile" ]; then
            echo "no $pngfile found, using placeholder.png now"
            pngfile="placeholder.png"
        fi
    fi

    pngfileen="${i}_en.png"
    if [ ! -f "$pngfileen" ]; then
        echo "no $pngfileen found, searching ${i}_en.jpg"
        pngfileen="${i}.jpg"
        if [ ! -f "$pngfileen" ]; then
            echo "no $pngfileen found, using same image as in german now"
            pngfileen=$pngfile
        fi
    fi

    echo "Handle $pngfile"
    convert -resize 200x200 $pngfile tmp.png
    convert tmp.png PNG:- | base64 > tmp
    tr -d '\n' < tmp > $ascfile

    sed -i -f - $newsletter << EOF
s%__IMGDATA_$(echo $i)__%$(cat $ascfile)%g
EOF

    if [ ! "$pngfile" == "$pngfileen" ]; then
        convert -resize 200x200 $pngfileen tmp.png
        convert tmp.png PNG:- | base64 > tmp
        tr -d '\n' < tmp > $ascfile
    fi
    sed -i -f - $newsletter << EOF
s%__IMGDATA_$(echo $i)_EN__%$(cat $ascfile)%g
EOF

    echo "inserting images $i done"

    textfile="${i}.html"
    sed -i -f - $newsletter << EOF
s%__TEXT_$(echo $i)__%$(cat $textfile)%g
EOF
    textfile="${i}_en.html"
    sed -i -f - $newsletter << EOF
s%__TEXT_$(echo $i)_EN__%$(cat $textfile)%g
EOF

    textfile="h${i}.txt"
    sed -i -f - $newsletter << EOF
s%__H_$(echo $i)__%$(cat $textfile)%g
EOF
    textfile="$h{i}_en.txt"
    sed -i -f - $newsletter << EOF
s%__H_$(echo $i)_EN__%$(cat $textfile)%g
EOF

    echo "iteration $i done"
done

echo "Finished inserting all announcments. Cleaning up."

rm tmp*
