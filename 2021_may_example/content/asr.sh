#!/bin/sh

tpl="newsletter.html.tpl"
newsletter="newsletter.html"

cp "$tpl" "$newsletter"

for i in c
do
    textfile="${i}.html"
    tr '\n' '~' < $textfile > tmp
    textfile="tmp"
    sed -i -f - $newsletter << EOF
s%__TEXT_$(echo $i)__%$(cat $textfile)%g
EOF

    textfile="${i}_en.html"
    tr '\n' '~' < $textfile > tmp
    textfile="tmp"
    sed -i -f - $newsletter << EOF
s%__TEXT_$(echo $i)_EN__%$(cat $textfile)%g
EOF

    tr '~' '\n' < $newsletter > tmp
    mv tmp "$newsletter"

    textfile="h${i}.txt"
    sed -i -f - $newsletter << EOF
s%__H_$(echo $i)__%$(cat $textfile)%g
EOF
    textfile="h${i}_en.txt"
    sed -i -f - $newsletter << EOF
s%__H_$(echo $i)_EN__%$(cat $textfile)%g
EOF

    echo "iteration $i done"
done

echo "Finished inserting all announcments."
