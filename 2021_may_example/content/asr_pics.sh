#!/bin/sh

tpl="newsletter.html.tpl"
newsletter="newsletter.html"

cp "$tpl" "$newsletter"

ascfile="tmp.asc"
for i in d
#for i in 8
do
    pngfile="${i}.png"
    if [ ! -f "$pngfile" ]; then
        echo "no $pngfile found, searching ${i}.jpg"
        pngfile="${i}.jpg"
        if [ ! -f "$pngfile" ]; then
            echo "no $pngfile found, using placeholder.png now"
            pngfile="placeholder.png"
        fi
    fi


    echo "Handle $pngfile"
    convert -resize 200x200 $pngfile tmp.png
    convert tmp.png PNG:- | base64 > tmp
    tr -d '\n' < tmp > $ascfile

    sed -i -f - $newsletter << EOF
s%__IMGDATA_$(echo $i)__%$(cat $ascfile)%g
EOF

    echo "inserting images $i done"
done

rm tmp*
